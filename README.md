# vue-cli

> A Vue.js project

Shows:

* multiple child Server components dynamically created in Servers component
  * pass server object to each Server component instance as props value
* eventBus component 
  * created in main.js
  * used to communicate between components not sharing an immediate common parent, where custom events might be more suitable
  * imported in Server (publisher) and ServerDetail (subscriber) components
  * on Server selection event, eventBus.$emit event
  * ServerDetail uses eventBus.$on at created lifecycle event to subsribe function to event
* button in ServerDetail sets status on server, which is a single object reference so change reflected anywhere it is used

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build
```

For detailed explanation on how things work, consult the [docs for vue-loader](http://vuejs.github.io/vue-loader).
